import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "../components/dashboard";
import Map from "../components/Map";
import Tryy from "../components/Tryy";
import Aff from "../components/Aff";
import Register from "../components/Register";
import App from "../components/App";

function Routes() {
  return (
    <Router>
      <Switch>
          <Route path={["/dashboard"]} exact component={Dashboard} />
          <Route path={["/fich"]} component={Tryy} />
          <Route path="/aff"   component={Aff} />
        <Route path={"/Register"} component={Register} />
        <Route path={["","/App"]} component={App} />

      </Switch>
    </Router>
  );
}

export default Routes;
