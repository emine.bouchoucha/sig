import React from 'react';
import request from '../utils/client.utils';

//signUp
export async function register(login){
    try {
        const response = await request({
            method: 'POST',
            url: '/auth/signup',
            data: login,
        });
        alert('success !');
                return response;

    } catch (error) {
      alert(error);
        throw (error.response || error.message);
    }
}
export async function signin(login){
    try{

        const response = await request({
            method: 'POST',
            url: '/auth/signin',
            data: login,
        });
        console.log("Bearer "+response.data.token);
        localStorage.setItem("token",JSON.stringify("Bearer "+response.data.token));
        alert('success !');
                return response;

    } catch (error) {
      alert(error);
        throw (error.response || error.message);
    }
}
