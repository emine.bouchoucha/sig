import request from '../utils/client.util';

async function converter(form){
    try {
        const response = await request({
            method: 'POST',
            url: 'http://ogre.adc4gis.com/convert',
            upload: form,
            headers: { 'Content-Type': 'multipart/form-data; boundary=---WebKitFormBoundary7MA4YWxkTrZu0gW' },

        });
        console.log(response);
        // localStorage.setItem("user" , JSON.stringify(response.data));
        return response;

    } catch (error) {
        throw (error.response || error.message);
    }
}

// export const AuthService = {converter:converter,}
export default converter;
