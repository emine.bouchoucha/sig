import request from '../utils/admin.util';
import http from "../http-common.js";
import React ,{ Component } from 'react';

export async function getFiles(){
    try {
      const response = await request({
            method: 'GET',
            url:"/filess",
        });
           return response.data;
    } catch (error) {
        throw (error.response || error.message);
    }
  }
  export async function deleteFile(id){
    try {
        const response = await request({
            method: 'DELETE',
            url:`/deletespatialdata/${id}`,
        });
           return response.data;
    } catch (error) {
        throw (error.response || error.message);
    }
  }
  export async function editFile(data){
    console.log(data);
    try {
        const response = await request({
            method: 'PUT',
            url:`/editspatialdata/${data.id}`,
            data:data
        });
           return response.data;
    } catch (error) {
        throw (error.response || error.message);
    }
  }
  export async function uploadData(data){
    console.log(data);
      try {
          const response = await request({
              method: 'POST',
              url:"/addspatialdata",
              data: data,
              dataType:'json',
              
                
          });
            return response;
      } catch (error) {
          throw (error.response || error.message);
      }
  }
  export async function uploadGeojson(data){
    console.log(data);
      try {
          const response = await request({
              method: 'POST',
              url:"/uploadGeojson",
              data: data,
              dataType:'json',
              
                
          });
            return response;
      } catch (error) {
          throw (error.response || error.message);
      }
  }

class UploadService {
//   constructor() {
    
//     this.state = {};
// }
  upload(file, onUploadProgress) {
    let formData = new FormData();

    formData.append("file", file);

    return http.post("/uploadd", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  }
  shapeFileProcessing(sFile){
    var formdata = new FormData();
    formdata.append("upload", sFile);
    console.log(formdata, sFile);
    console.log('hii');
    $.ajax({
        url: 'https://ogre.adc4gis.com/convert',
        data: formdata,
        type: "POST",
        processData: false,
        contentType: false,
        success: function(msg) {
            console.log("Success: "+msg);
            // this.setState({
            //   GeojsonData: msg});
              // console.log(this.GeojsonData);
              uploadGeojson(msg);
            // document.getElementById("result").innerHTML = JSON.stringify(msg);
        },
        error: function(msg) {
            console.log("Error: "+msg);
        }
    });
}
//mongosh "mongodb+srv://cluster1.tq07t.mongodb.net/sigdatabase" --username sig --password test


  getFiles() {
    return http.get("/findall");
  }
}
export default new UploadService();

