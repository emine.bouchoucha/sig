import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routes';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'leaflet/dist/leaflet.css';
import Map from "./components/Map";

ReactDOM.render(
  <React.StrictMode>
    <Routes />
  </React.StrictMode>,
  document.getElementById('root')
);

