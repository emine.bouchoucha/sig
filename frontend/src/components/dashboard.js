import React , {useEffect,useState} from "react";
import { getFiles,deleteFile} from "../services/admin.service";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import AddFile from './addFile';
import EditFile from './editFile';
import {Add, Delete,Edit,ExitToApp}from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import Map from "./Map";
import Tryy from "./Tryy";
import JSZip from "jszip";
import { saveAs } from 'file-saver';



const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function Dashboard() {
const [files,setFiles]=useState([]);
const [open, setOpen] = React.useState(false);
const handleOpen = () => setOpen(true);
const handleClose = () => setOpen(false);

const [openEdit, setOpenEdit] = React.useState(false);
const [file, setFile]=React.useState();
const handleOpenEdit = () => setOpenEdit(true);
const handleCloseEdit = () => setOpenEdit(false);

const deleteHandler=()=>{
  deleteFile(data.id)
  .then((response) => {
    console.log('file deleted');
     window.location.reload();
  })
  .catch((error) => {
    console.log(error);
  });
};
function base64ToBuffer(str){
    str = window.atob(str); // creates a ASCII string
    var buffer = new ArrayBuffer(str.length),
        view = new Uint8Array(buffer);
    for(var i = 0; i < str.length; i++){
        view[i] = str.charCodeAt(i);
    }
    return buffer;
}
function shapeFileProcessing(file){
  console.log(file);
  file = new File([new ArrayBuffer(file)],"ft.zip",{
    type : "application/zip"
  });
  console.log(file);
  var formdata = new FormData();
  formdata.append("upload", file);
  console.log(file);
  $.ajax({
      url: 'https://ogre.adc4gis.com/convert',
      data: formdata,
      type: "POST",
      processData: false,
      contentType: false,
      success: function(msg) {
          return msg;
          // document.getElementById("result").innerHTML = JSON.stringify(msg);
      },
      error: function(msg) {
          console.log("Error: "+JSON.stringify(msg));

      }
  });
}
  useEffect(() => {
    getFiles()
      .then((response) => {
        console.log(response);
        setFiles(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const[data,setData]=useState("");
  const[datageojson,setDatageojson]=useState({});


  function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  function loadData(data){
    setData(data);
    setDatageojson(shapeFileProcessing(data.file.fileData));
   
  }

  const slider= files?files.map((item,index) => (                        
    <div className="item-container"  key={index}  onClick={()=>loadData(item)}>
          <img 
              className="image"
              src={`/img/${randomIntFromInterval(1, 10)}.jfif`}
              loading="lazy"
              alt={item.title}

          />
            <div className="over-image">
              <p>{item.title}</p>
            </div>
    </div> 
  )):'';
    return(
            <>
                    <div className="navbar navbar-light bg-light shadow">
                        <a className="brand d-flex align-items-center justify-content-center">
                            <div className=" brand-icon fa-lg">
                                <i className=" fas fa-globe-africa"></i>
                            </div>
                            <div className="brand-text">GEO BACKEND</div>
                        </a>
                        <tr>
                          <th>
                              <form
                                  className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                                  <div className="input-group">
                                      <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..."
                                          aria-label="Search" aria-describedby="basic-addon2"/>
                                      <div className="input-group-append">
                                          <button className="btn btn-default" type="button">
                                              <i className="fas fa-search fa-sm"></i>
                                          </button>
                                      </div>
                                  </div>
                              </form>
                        </th>
                        <th ><Button onClick={handleOpen}><Add></Add> Add File</Button></th>
                        <th ><Button><ExitToApp></ExitToApp>Logout</Button></th>
                            </tr>
                      
                        
    
                    </div>
                  
                    
                    <div className="container-fluid">   
                        <div className="scroll__container">
                        {slider}
                    </div>
                   {files?(
                    <div className="row justify-content-center map__container">
                       
                        <div className=" mr description box-shadow" >
                        <div className="icons">
                              <Delete onClick={deleteHandler}></Delete>
                              <Edit onClick={handleOpenEdit}></Edit>
                              <Modal
                              key='2'
                              open={openEdit}
                              onClose={handleCloseEdit}
                              aria-labelledby="modal-modal-title"
                              aria-describedby="modal-modal-description"
                            >
                              <Box sx={style}>
                              <EditFile props={data} ></EditFile>
                              </Box>
                            </Modal>
                          </div>
                            <div className="description-header"> Description  </div>




                            <Modal
                              key='1'
                              open={open}
                              onClose={handleClose}
                              aria-labelledby="modal-modal-title"
                              aria-describedby="modal-modal-description"
                            >
                              <Box sx={style}>
                                <AddFile></AddFile>
                              </Box>
                            </Modal>

                            <div>
                                {data.description}
                            </div>
                        </div>

                        </div>
                      ):''
                    }
                      </div>

                    
                <Map Datageojson />

                </>
  
 
    );
}

export default Dashboard;
