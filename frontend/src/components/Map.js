import React, { Component } from 'react';
import L from 'leaflet';
// postCSS import of Leaflet's CSS
import 'leaflet/dist/leaflet.css';
// using webpack json loader we can import our geojson file like this
// import geojson from './bk_subway_entrances.geojson';
// import local components Filter and ForkMe
import Filter from './Filter';
import ForkMe from './ForkMe';
import Dashboard from "./dashboard";
import ReactDOM from 'react-dom';

// store the map configuration properties in an object,
// we could also move this to a separate file & import it if desired.
let config = {};
config.params = {
  center: [40.655769,-73.938503],
  zoomControl: false,
  zoom: 13,
  maxZoom: 19,
  minZoom: 11,
  scrollwheel: false,
  legends: true,
  infoControl: false,
  attributionControl: true
};
config.tileLayer = {
  //noir
  //uri: 'http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
  //mrigla
  uri: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  //mrigla
  //uri:             'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  //uri: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

  params: {
    minZoom: 11,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
    id: '',
    accessToken: ''
  }
};
var geojson={
  "type": "FeatureCollection",
  "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },

  "features": [
    { "type": "Feature", "properties": { "NAME": "Smith St & Bergen St At Ne Corner (To Manhattan And Queens Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "F-G" }, "geometry": { "type": "Point", "coordinates": [ -73.990271999344998, 40.686727997981862 ] } },
    { "type": "Feature", "properties": { "NAME": "Court St & Montague St At Sw Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3-4-5-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.990591999101738, 40.693641998305573 ] } },
    { "type": "Feature", "properties": { "NAME": "Clinton St & Montague St At Nw Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3-4-5-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.992537356702158, 40.694392786197241 ] } },
    { "type": "Feature", "properties": { "NAME": "Flatbush Ave & Empire Blvd At Sw Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "B-Q-S" }, "geometry": { "type": "Point", "coordinates": [ -73.96224891524794, 40.662727024905131 ] } },
    { "type": "Feature", "properties": { "NAME": "4th Ave & Union St At Sw Corner (To Bay Ridge And Coney Island Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.983394168605855, 40.67716401982004 ] } },
    { "type": "Feature", "properties": { "NAME": "4th Ave & Union St At Se Corner (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.983082312481287, 40.677051302497773 ] } },
    { "type": "Feature", "properties": { "NAME": "586 Lafayette Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "G" }, "geometry": { "type": "Point", "coordinates": [ -73.951606058173098, 40.689684367225709 ] } },
    { "type": "Feature", "properties": { "NAME": "657 Carlton Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "B-Q" }, "geometry": { "type": "Point", "coordinates": [ -73.972240619684385, 40.67732278750551 ] } },
    { "type": "Feature", "properties": { "NAME": "462 Flatbush Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "B-Q-S" }, "geometry": { "type": "Point", "coordinates": [ -73.962284759698591, 40.662482484562886 ] } },
    { "type": "Feature", "properties": { "NAME": "3327 Fulton Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J-Z" }, "geometry": { "type": "Point", "coordinates": [ -73.872847000078551, 40.683585999921213 ] } },
    { "type": "Feature", "properties": { "NAME": "692 Parkside Avenue (To Flatbush Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-5" }, "geometry": { "type": "Point", "coordinates": [ -73.950349998317407, 40.656148999987558 ] } },
    { "type": "Feature", "properties": { "NAME": "1842 Nostrand Avenue (To Flatbush Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-5" }, "geometry": { "type": "Point", "coordinates": [ -73.94864900101301, 40.640745001131322 ] } },
    { "type": "Feature", "properties": { "NAME": "902 8 Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "F" }, "geometry": { "type": "Point", "coordinates": [ -73.979491001133752, 40.665676999558933 ] } },
    { "type": "Feature", "properties": { "NAME": "1289 Fulton Street (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C" }, "geometry": { "type": "Point", "coordinates": [ -73.949748978775915, 40.680541693812188 ] } },
    { "type": "Feature", "properties": { "NAME": "4910 New Utrecht Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D" }, "geometry": { "type": "Point", "coordinates": [ -73.994948000946252, 40.636392998227038 ] } },
    { "type": "Feature", "properties": { "NAME": "7102 16 Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D" }, "geometry": { "type": "Point", "coordinates": [ -73.998936000353268, 40.61876800130878 ] } },
    { "type": "Feature", "properties": { "NAME": "170 Marcy Avenue (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J-M-Z" }, "geometry": { "type": "Point", "coordinates": [ -73.957946664071159, 40.708714871351283 ] } },
    { "type": "Feature", "properties": { "NAME": "957 Fulton Street (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C" }, "geometry": { "type": "Point", "coordinates": [ -73.964628885557588, 40.683243102175837 ] } },
    { "type": "Feature", "properties": { "NAME": "2222 Pitkin Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C" }, "geometry": { "type": "Point", "coordinates": [ -73.890386001804956, 40.672500999449696 ] } },
    { "type": "Feature", "properties": { "NAME": "2084 Fulton Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C" }, "geometry": { "type": "Point", "coordinates": [ -73.913667999218958, 40.678279001579995 ] } },
    { "type": "Feature", "properties": { "NAME": "104 Van Siclen Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J-Z" }, "geometry": { "type": "Point", "coordinates": [ -73.891730000781237, 40.677756000466836 ] } },
    { "type": "Feature", "properties": { "NAME": "956 Mc Donald Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "F" }, "geometry": { "type": "Point", "coordinates": [ -73.97734022841172, 40.630418029319785 ] } },
    { "type": "Feature", "properties": { "NAME": "1713 Church Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "B-Q" }, "geometry": { "type": "Point", "coordinates": [ -73.963512542015835, 40.649400108495989 ] } },
    { "type": "Feature", "properties": { "NAME": "1518 Avenue H", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "B-Q" }, "geometry": { "type": "Point", "coordinates": [ -73.961476890564668, 40.62992973441596 ] } },
    { "type": "Feature", "properties": { "NAME": "1551 71 Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D" }, "geometry": { "type": "Point", "coordinates": [ -73.999178000786159, 40.61919700025053 ] } },
    { "type": "Feature", "properties": { "NAME": "214 Cleveland Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J" }, "geometry": { "type": "Point", "coordinates": [ -73.885710000991509, 40.679697999583716 ] } },
    { "type": "Feature", "properties": { "NAME": "3000 Church Avenue (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-5" }, "geometry": { "type": "Point", "coordinates": [ -73.949250859839623, 40.650518367277499 ] } },
    { "type": "Feature", "properties": { "NAME": "183 Bedford Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "L" }, "geometry": { "type": "Point", "coordinates": [ -73.957576997797617, 40.717856000591979 ] } },
    { "type": "Feature", "properties": { "NAME": "623 Manhattan Avenue", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "G" }, "geometry": { "type": "Point", "coordinates": [ -73.95085799878089, 40.723660999778112 ] } },
    { "type": "Feature", "properties": { "NAME": "95 Bushwick Avenue (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "L" }, "geometry": { "type": "Point", "coordinates": [ -73.940578000649452, 40.712130001348704 ] } },
    { "type": "Feature", "properties": { "NAME": "1 Boerum Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "G" }, "geometry": { "type": "Point", "coordinates": [ -73.950058562953885, 40.705581864144278 ] } },
    { "type": "Feature", "properties": { "NAME": "275 Kings Highway", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "N" }, "geometry": { "type": "Point", "coordinates": [ -73.980424790683031, 40.605338592367147 ] } },
    { "type": "Feature", "properties": { "NAME": "302 Broadway (To Queens And Canarsie Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J-M-Z" }, "geometry": { "type": "Point", "coordinates": [ -73.957868999699386, 40.708231001537676 ] } },
    { "type": "Feature", "properties": { "NAME": "1 Greene Avenue (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C" }, "geometry": { "type": "Point", "coordinates": [ -73.97327999957497, 40.686078000165722 ] } },
    { "type": "Feature", "properties": { "NAME": "368 Jay Street", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "A-C-F" }, "geometry": { "type": "Point", "coordinates": [ -73.987513023472275, 40.69239954985418 ] } },
    { "type": "Feature", "properties": { "NAME": "394 Broadway", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "J-M" }, "geometry": { "type": "Point", "coordinates": [ -73.954371000030463, 40.707073999033611 ] } },
    { "type": "Feature", "properties": { "NAME": "2848 Church Avenue (To Flatbush Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-5" }, "geometry": { "type": "Point", "coordinates": [ -73.949721000110813, 40.650662999414116 ] } },
   
    { "type": "Feature", "properties": { "NAME": "4th Ave & Union St At Se Corner (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "D-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.982989996894915, 40.677194757250753 ] } },
    { "type": "Feature", "properties": { "NAME": "Flatbush Ave & Bergen St At Sw Corner (To New Lots And Flatbush Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3-4" }, "geometry": { "type": "Point", "coordinates": [ -73.975208999782225, 40.680698998820716 ] } },
    { "type": "Feature", "properties": { "NAME": "Lawrence St & Willoughby St At Ne Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.986214643563116, 40.692043780335773 ] } },
    { "type": "Feature", "properties": { "NAME": "Lawrence St & Willoughby St At Se Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.986113771109885, 40.692258082753781 ] } },
    { "type": "Feature", "properties": { "NAME": "Hoyt St & Fulton St At Sw Corner (To New Lots And Flatbush Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3" }, "geometry": { "type": "Point", "coordinates": [ -73.985271002038431, 40.69044899949936 ] } },
    { "type": "Feature", "properties": { "NAME": "Bridge St & Fulton St At Ne Corner (To Manhattan Only)", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3" }, "geometry": { "type": "Point", "coordinates": [ -73.985255000987408, 40.690895000985456 ] } },
    { "type": "Feature", "properties": { "NAME": "Court St & Montague St At Sw Corner", "URL": "http:\/\/www.mta.info\/nyct\/service\/", "LINE": "2-3-4-5-N-R" }, "geometry": { "type": "Point", "coordinates": [ -73.990678000925513, 40.693724998853824 ] } },
  ]};
// array to store unique names of Brooklyn subway lines,
// this eventually gets passed down to the Filter component
let subwayLineNames = [];

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null,
      tileLayer: null,
      geojsonLayer: null,
      geojson: null,
      subwayLinesFilter: '*',
      numEntrances: '',
      geojson: this.props.Datageojson

    };
    this._mapNode = null;
    this.updateMap = this.updateMap.bind(this);
    this.onEachFeature = this.onEachFeature.bind(this);
    this.pointToLayer = this.pointToLayer.bind(this);
    this.filterFeatures = this.filterFeatures.bind(this);
    this.filterGeoJSONLayer = this.filterGeoJSONLayer.bind(this);
  }

  componentDidMount() {
    // code to run just after the component "mounts" / DOM elements are created
    // we could make an AJAX request for the GeoJSON data here if it wasn't stored locally

    this.getData();
    // create the Leaflet map object
    if (!this.state.map) this.init(this._mapNode);
  }

  componentDidUpdate(prevProps, prevState) {
    // code to run when the component receives new props or state
    // check to see if geojson is stored, map is created, and geojson overlay needs to be added
    if (this.state.geojson && this.state.map && !this.state.geojsonLayer) {
      // add the geojson overlay
      this.addGeoJSONLayer(this.state.geojson);
    }

    // check to see if the subway lines filter has changed
    if (this.state.subwayLinesFilter !== prevState.subwayLinesFilter) {
      // filter / re-render the geojson overlay
      this.filterGeoJSONLayer();
    }
  }

  componentWillUnmount() {
    // code to run just before unmounting the component
    // this destroys the Leaflet map object & related event listeners
    this.state.map.remove();
  }

  getData() {
    // could also be an AJAX request that results in setting state with the geojson data
    // for simplicity sake we are just importing the geojson data using webpack's json loader
    this.setState({
      numEntrances: geojson.features.length,
      geojson
    });
  }

  updateMap(e) {
    let subwayLine = e.target.value;
    // change the subway line filter
    if (subwayLine === "All lines") {
      subwayLine = "*";
    }
    // update our state with the new filter value
    this.setState({
      subwayLinesFilter: subwayLine
    });
  }

  addGeoJSONLayer(geojson) {
    // create a native Leaflet GeoJSON SVG Layer to add as an interactive overlay to the map
    // an options object is passed to define functions for customizing the layer

    const geojsonLayer = L.geoJson(geojson, {
      onEachFeature: this.onEachFeature,
      pointToLayer: this.pointToLayer,
      filter: this.filterFeatures
    });
    // add our GeoJSON layer to the Leaflet map object
    geojsonLayer.addTo(this.state.map);

    // store the Leaflet GeoJSON layer in our component state for use later
    this.setState({ geojsonLayer });
    // fit the geographic extent of the GeoJSON layer within the map's bounds / viewport
    this.zoomToFeature(geojsonLayer);
  }

  filterGeoJSONLayer() {
    // clear the geojson layer of its data
    this.state.geojsonLayer.clearLayers();
    // re-add the geojson so that it filters out subway lines which do not match state.filter
    this.state.geojsonLayer.addData(geojson);
    // fit the map to the new geojson layer's geographic extent
    this.zoomToFeature(this.state.geojsonLayer);
  }

  zoomToFeature(target) {
    // pad fitBounds() so features aren't hidden under the Filter UI element
    var fitBoundsParams = {
      paddingTopLeft: [200,10],
      paddingBottomRight: [10,10]
    };
    // set the map's center & zoom so that it fits the geographic extent of the layer
    this.state.map.fitBounds(target.getBounds(), fitBoundsParams);
  }

  filterFeatures(feature, layer) {
    // filter the subway entrances based on the map's current search filter
    // returns true only if the filter value matches the value of feature.properties.LINE
    const test = feature.properties.LINE.split('-').indexOf(this.state.subwayLinesFilter);
    if (this.state.subwayLinesFilter === '*' || test !== -1) {
      return true;
    }
  }

  pointToLayer(feature, latlng) {
    // renders our GeoJSON points as circle markers, rather than Leaflet's default image markers
    // parameters to style the GeoJSON markers
    var markerParams = {
      radius: 4,
      fillColor: 'orange',
      color: '#fff',
      weight: 1,
      opacity: 0.5,
      fillOpacity: 0.8
    };

    return L.circleMarker(latlng, markerParams);
  }

  onEachFeature(feature, layer) {
    if (feature.properties && feature.properties.NAME && feature.properties.LINE) {

      // if the array for unique subway line names has not been made, create it
      // there are 19 unique names total
      if (subwayLineNames.length < 19) {

        // add subway line name if it doesn't yet exist in the array
        feature.properties.LINE.split('-').forEach(function(line, index){
          if (subwayLineNames.indexOf(line) === -1) subwayLineNames.push(line);
        });

        // on the last GeoJSON feature
        if (this.state.geojson.features.indexOf(feature) === this.state.numEntrances - 1) {
          // use sort() to put our values in alphanumeric order
          subwayLineNames.sort();
          // finally add a value to represent all of the subway lines
          subwayLineNames.unshift('All lines');
        }
      }

      // assemble the HTML for the markers' popups (Leaflet's bindPopup method doesn't accept React JSX)
      const popupContent = `<h3>${feature.properties.NAME}</h3>
        <strong>Access to MTA lines: </strong>${feature.properties.LINE}`;

      // add our popups
      layer.bindPopup(popupContent);
    }
  }

  init(id) {
    if (this.state.map) return;
    // this function creates the Leaflet map object and is called after the Map component mounts
    let map = L.map(id, config.params);
    L.control.zoom({ position: "bottomleft"}).addTo(map);
    L.control.scale({ position: "bottomleft"}).addTo(map);

    // a TileLayer is used as the "basemap"
    const tileLayer = L.tileLayer(config.tileLayer.uri, config.tileLayer.params).addTo(map);

    // set our state to include the tile layer
    this.setState({ map, tileLayer });
  }

  render() {
    const { subwayLinesFilter } = this.state;

    return (
          <div id="mapUI">
            {
              /* render the Filter component only after the subwayLines array has been created */
              subwayLineNames.length &&
              <Filter lines={subwayLineNames}
                      curFilter={subwayLinesFilter}
                      filterLines={this.updateMap} />
            }
            <div ref={(node) => this._mapNode = node} id="map" />
            <ForkMe />
  </div>

    );
  }
}

export default Map;
