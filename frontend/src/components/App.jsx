import React from 'react';
import '../index.css';
import "bootstrap/dist/css/bootstrap.css";
import {Button, Alert, Row, Col, Image} from 'react-bootstrap';
import Login from "./Login";
import Menu from "./Menu";
import bg from "../assets/bg.jpg";
function App() {
  return (
    <div className="App">
      <Image src={bg}  className="bg"/>

      <Row className="landing card-img-overlay ">
        <Col sm={3}></Col>
        <Col sm={6} ><Login /></Col>


      </Row>
    </div>
  );
}
//
export default App;
