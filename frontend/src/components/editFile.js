import React ,{ Component } from 'react';
import UploadService ,{uploadData,editFile}from "../services/admin.service";

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import PublishIcon from '@material-ui/icons/Publish';
class EditFile extends Component {
  constructor(props) {
    super(props);
    console.log(props.props.id);
    this.selectFile = this.selectFile.bind(this);
    this.upload = this.upload.bind(this);

    this.state = {
      selectedFiles: undefined,
      currentFile: undefined,
      progress: 0,
      message: "",
      fileInfos: [],
      data:{ 
        ...this.props.props,
        title:this.props.props.title,
        description: this.props.props.description,
        owner: this.props.props.owner,
        fileID:this.props.props.fileID,
    }
    };
    this.handleChange=this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
   


  selectFile(event) {
    this.setState({
      selectedFiles: event.target.files,
    });
  }

  upload() {
    let currentFile = this.state.selectedFiles[0];

    this.setState({
      progress: 0,
      currentFile: currentFile,
    });

    UploadService.upload(currentFile, (event) => {
      this.setState({
        progress: Math.round((100 * event.loaded) / event.total),
      });
    })
      .then((response) => {
        console.log("lalalalallalal");
        console.log(response.data);
        this.setState({
          data:{...this.state.data,fileID:response.data},
          message: response.data.message,
        });
        
      })
      .then((files) => {
        this.setState({
          fileInfos: files.data,
        });
      })
      .catch(() => {
        this.setState({
          progress: 0,
          message: "Could not upload the file!",
          currentFile: undefined,
        });
      });

    this.setState({
      selectedFiles: undefined,
    });
  }

  handleChange(e){
    this.setState({
        data:{
            ...this.state.data,
        [e.target.id]: e.target.value
        }
    });
  };
  handleSubmit(){
        console.log(this.state.data);
        editFile(this.state.data)
        .then((response) => {     
          console.log(response);
          window.location.reload();
          })
        .catch((error) => {
            console.log(error);
          });
  }
  render() {
    const {
      selectedFiles,
      currentFile,
      progress,
      message,
      fileInfos,
    } = this.state;
    const{title,description,owner}=this.state.data;
    return (

        <div className="row ">
        <div className="p-5">
            <div className="text-center">
                <h1 className="h4 text-gray-900 mb-4">Edit new file</h1>
            </div>
            <form className="user">
                <div className="form-group">
                    <input type="text" className="form-control form-control-user" id="title"
                        placeholder="Title"
                        value={title}
                        onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control form-control-user" id="description"
                        placeholder="Description"
                        value={description}
                        onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control form-control-user" id="owner"
                        placeholder="Owner"
                        value={owner}
                        onChange={this.handleChange}/>
                </div>
                       <div>
        {/* {currentFile && (
        //   <div className="progress">
        //     <div
        //       className="progress-bar progress-bar-info progress-bar-striped"
        //       role="progressbar"
        //       aria-valuenow={progress}
        //       aria-valuemin="0"
        //       aria-valuemax="100"
        //       style={{ width: progress + "%" }}
        //     >
        //       {progress}%
        //     </div>
        //   </div>
       
      )} */}

        {/* <label className="btn btn-default">
          <input type="file" onChange={this.selectFile} />
        </label>

        <button
          className="btn btn-success"
          disabled={!selectedFiles}
          onClick={this.upload}
        >
          Upload
        </button>

        <div className="alert alert-light" role="alert">
          {message}
        </div> */}

        <div>
      <input
        className="input"
        id="contained-button-file"
        multiple
        type="file"
        style={{display:'none'}}
        onChange={this.selectFile}
      />
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span"
        
        >
          Choose file
        </Button>
      </label>
      <label htmlFor="icon-button-file">
        <IconButton color="primary" aria-label="upload" component="span" disabled={!selectedFiles} onClick={this.upload}>
          <PublishIcon />
        </IconButton>
      </label>
    </div>
      </div>
   
                <a  class="btn btn-secondary  btn-user btn-block" onClick={this.handleSubmit}>
                                          Edit
                                      </a>
            </form>
         
        </div>
</div>

  );
  }
}
export default EditFile;
