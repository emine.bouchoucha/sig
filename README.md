# Map Manager
Our application provides an interactive dashboard dedicated to managers and directors to manage shp files, maps and visualize access points directly on a map

**Realized by :**
- Emine Bouchoucha
- Arij El Korbi
- Fatma Chtioui
- Achref Trabelsi
- Ahmed Jallouli
- Sana Anas

**App Features**
- Authentication
- Visualize shp files
- Manage shp files
- Search projects by name or date

# About the App

**Technologies used :**

- Spring boot
- React JS
- Docker
- PostgreSQL
- Ogre API (https://ogre.adc4gis.com/)

**How to use the App :**
- install postgreSQL on your machine:

sudo apt-get install postgresql postgresql-contrib
sudo apt install pgadmin4 

- clone the repository :
git clone https://gitlab.com/emine.bouchoucha/sig.git

- run frontend :

cd frontend/
npm install
npm start

- run backend :

cd Backend/
mvn install
mvn spring-boot:run
# ScreenShots

**Login screen**
![1](https://gitlab.com/emine.bouchoucha/sig/-/raw/main/sig1.png)

**Register screen**
![1](https://gitlab.com/emine.bouchoucha/sig/-/raw/main/sig2.png)

**File List screen**
![1](https://gitlab.com/emine.bouchoucha/sig/-/raw/main/1.JPG)


**Add file screen**
![1](https://gitlab.com/emine.bouchoucha/sig/-/raw/main/2.JPG)


**Dashboard screen**

![1](https://gitlab.com/emine.bouchoucha/sig/-/raw/main/3.JPG)

