package com.geobackend.backend.controllers;

import com.geobackend.backend.Response.FileResponse;
import com.geobackend.backend.constants.constants;
import com.geobackend.backend.entities.FileModel;
import com.geobackend.backend.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import org.springframework.core.io.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
@CrossOrigin
@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    FileService fileService;
    @CrossOrigin(origins = constants.BaseURL)

    @PostMapping("/upload")
    public UUID uploadFile(@RequestParam("file") MultipartFile file) {

        FileModel model = fileService.saveFile(file);
        String fileUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/download/").
                path(String.valueOf(model.getFileId())).toUriString();
        return model.getFileId();
    }

    /*@PostMapping("/uploadMultipleFiles")
    public List<FileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files).
                stream().
                map(file -> uploadFile(file)).
                collect(Collectors.toList());
    }*/
     /*@CrossOrigin(origins = constants.BaseURL)
   @GetMapping("/download/{fileName}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName) {
        FileModel model = fileService.getFile(fileName);
        return ResponseEntity.ok().
                header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + model.getFileName() + "\"").
                body(new ByteArrayResource(model.getFileData()));


    }*/

    @CrossOrigin(origins = constants.BaseURL)
    @GetMapping("/allfiles")
    public List<FileModel> getListFiles(Model model) {
        List<FileModel> fileDetails = fileService.getListOfFiles();

        return fileDetails;
    }
}
