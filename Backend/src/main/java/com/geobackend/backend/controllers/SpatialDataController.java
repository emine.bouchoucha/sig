package com.geobackend.backend.controllers;

import com.geobackend.backend.constants.constants;
import com.geobackend.backend.entities.FileModel;
import com.geobackend.backend.entities.JsonData;
import com.geobackend.backend.entities.SpatialData;
import com.geobackend.backend.payload.request.SpatialDataRequest;

import com.geobackend.backend.services.FileService;
import com.geobackend.backend.services.JsonDataService;
import com.geobackend.backend.services.SpatialDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class SpatialDataController {
    @Autowired
    private FileService fileService;
    @Autowired
    private SpatialDataService spatialDataService;
    @Autowired
    private JsonDataService jsonDataService;
    @CrossOrigin(origins = constants.BaseURL ,allowedHeaders="*")
    @PostMapping("/addspatialdata")

    public SpatialData addSpatialData ( @RequestBody SpatialDataRequest spatialDataRequest ) {

        SpatialData spatialData = new SpatialData(spatialDataRequest.getTitle(),spatialDataRequest.getDescription(),spatialDataRequest.getOwner(),spatialDataRequest.getDateOfOperation(),
                spatialDataRequest.getDateOfModification());
        FileModel fileObj=fileService.getFile(spatialDataRequest.getFileID());
        spatialData.setFile(fileObj);
        spatialDataService.saveSpatialData(spatialData);
        return spatialData;
    }
    @CrossOrigin(origins = constants.BaseURL ,allowedHeaders="*")
    @PostMapping("/uploaddata")

    public JsonData addSpatialData (@RequestBody JsonData jsonData ) {

        JsonData jsonDataObj = null;

        jsonDataObj=jsonDataService.saveJsonData(jsonData);
        return jsonDataObj;
    }
    @GetMapping("/findall")
    @CrossOrigin(origins = constants.BaseURL,allowedHeaders="*")
    public List<SpatialData> findAllSpatialData() throws Exception{
        System.out.println("debut ");
        List<SpatialData> spatialDataList=spatialDataService.findAllSpatialData();
        return spatialDataList;
    }
    @GetMapping("/spatialdata/{id}")
    @CrossOrigin(origins = constants.BaseURL,allowedHeaders="*")
    public SpatialData findOrderById(@PathVariable(value = "id") int id) {
        SpatialData spatialData = spatialDataService.findSpatialDataById(id);
        return spatialData;
    }

    @DeleteMapping("/deletespatialdata/{id}")
    @CrossOrigin(origins = constants.BaseURL,allowedHeaders="*")
    public String deleteSpatialData(@PathVariable(value = "id") int id) {
        spatialDataService.deleteSpatialData(id);
        return "deleted";
    }
    @PutMapping("/editspatialdata/{id}")
    @CrossOrigin(origins = constants.BaseURL,allowedHeaders="*")
    public SpatialData updateSpatialData(@PathVariable int id, @RequestBody SpatialDataRequest spatialDataRequest) throws Exception {
        SpatialData spatialData = spatialDataService.findSpatialDataById(id);
        if (spatialData == null) {
            throw new Exception("spatial  with" + id + "doesn't exist");
        }
        spatialData.setTitle(spatialDataRequest.getTitle());
        spatialData.setOwner(spatialDataRequest.getOwner());
        spatialData.setDescription(spatialDataRequest.getDescription());
        spatialData.setDateOfOperation(spatialDataRequest.getDateOfOperation());
        spatialData.setDateOfModification(spatialDataRequest.getDateOfModification());
        FileModel fileObj=fileService.getFile(spatialDataRequest.getFileID());
     spatialData.setFile(fileObj);
        SpatialData updatedSpatialData = spatialDataService.saveSpatialData(spatialData);
        return updatedSpatialData;
    }

}
