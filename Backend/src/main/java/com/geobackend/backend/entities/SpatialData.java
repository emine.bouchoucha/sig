package com.geobackend.backend.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "spatialData")
public class SpatialData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    private String owner;
    private Date dateOfOperation;
    private Date dateOfModification;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fileId")
    private FileModel file;

    public SpatialData() {

    }

    public SpatialData(String title, String description, String owner, Date dateOfOperation, Date dateOfModification) {
        this.title = title;
        this.description = description;
        this.owner = owner;
        this.dateOfOperation = dateOfOperation;
        this.dateOfModification=dateOfModification;
    }

    public Date getDateOfModification() {
        return dateOfModification;
    }

    public void setDateOfModification(Date dateOfModification) {
        this.dateOfModification = dateOfModification;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getDateOfOperation() {
        return dateOfOperation;
    }

    public void setDateOfOperation(Date dateOfOperation) {
        this.dateOfOperation = dateOfOperation;
    }
}
