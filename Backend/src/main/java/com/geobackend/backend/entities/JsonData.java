package com.geobackend.backend.entities;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "jsonData")
public class JsonData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String type;
    private String  crs;
    @ElementCollection
    private List<String> data;


    public JsonData() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return  name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String fileType) {
        this.type = fileType;
    }


    public List<String> getData() {
        return data;
    }

    public void setData(List<String> fileType) {
        this.data = fileType;
    }
    public String getCrs() {
        return crs;
    }

    public void setCrs(String crs) {
        this.crs= crs;
    }
}
