package com.geobackend.backend.services;

import java.util.ArrayList;
import java.util.List;

import com.geobackend.backend.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geobackend.backend.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public void addUser(Utilisateur utilisateur) {
		userRepository.save(utilisateur);
	}
	public Utilisateur getUser(Long id) {
		return userRepository.findById(id).get();
	}
	public List<Utilisateur> getAllUsers() {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		userRepository.findAll().forEach(utilisateurs::add);
		return utilisateurs;
	}
	public void updateUser(Utilisateur utilisateur) {
		userRepository.save(utilisateur);
	}
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}

}
