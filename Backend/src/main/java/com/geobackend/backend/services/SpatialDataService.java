package com.geobackend.backend.services;

import com.geobackend.backend.entities.SpatialData;
import com.geobackend.backend.repository.SpatialDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional
@Service
public class SpatialDataService {
    @Autowired
    SpatialDataRepo spatialDataRepo;
    public SpatialData saveSpatialData(SpatialData spatialData) { return spatialDataRepo.save(spatialData) ; }
    public List<SpatialData> findAllSpatialData(){ return spatialDataRepo.findAll(); }
    public SpatialData findSpatialDataById(int id) {return spatialDataRepo.findSpatialDataById(id);}
    public void deleteSpatialData (int id){ spatialDataRepo.deleteById(id);}
}
