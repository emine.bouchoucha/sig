package com.geobackend.backend.services;

import com.geobackend.backend.entities.JsonData;
import com.geobackend.backend.repository.JsonDataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JsonDataService {
    @Autowired
    private JsonDataRepo repo;
    public JsonData saveJsonData(JsonData profile) { return repo.save(profile); }
    public JsonData findRoleById (int id){ return repo.findJsonDataById(id); }

}