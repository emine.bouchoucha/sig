package com.geobackend.backend.services;

import java.util.Objects;

import com.geobackend.backend.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.geobackend.backend.entities.JwtRequest;
import com.geobackend.backend.repository.UserRepository;

@Service
public class AuthenticationService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;


	public void authenticate(JwtRequest authRequest) throws Exception {
		Objects.requireNonNull(authRequest.getEmail());
		Objects.requireNonNull(authRequest.getPassword());

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getEmail(),authRequest.getPassword()));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	public void signUp(JwtRequest authRequest) {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setEmail(authRequest.getEmail());
		utilisateur.setPassword(passwordEncoder.encode(authRequest.getPassword()));
		userRepository.save(utilisateur);
	}

}
