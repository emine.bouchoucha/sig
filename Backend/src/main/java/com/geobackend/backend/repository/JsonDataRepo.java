package com.geobackend.backend.repository;

import com.geobackend.backend.entities.JsonData;
import com.geobackend.backend.entities.SpatialData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JsonDataRepo extends JpaRepository<JsonData, Integer> {
    public JsonData findJsonDataById(Integer id);


}