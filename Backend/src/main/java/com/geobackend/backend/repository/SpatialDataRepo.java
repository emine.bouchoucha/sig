package com.geobackend.backend.repository;

import com.geobackend.backend.entities.SpatialData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpatialDataRepo extends JpaRepository<SpatialData, Integer> {
    public SpatialData findSpatialDataById(int id);
    public void deleteById(int id);

}
