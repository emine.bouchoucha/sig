package com.geobackend.backend.repository;

import com.geobackend.backend.entities.FileModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface FileRepo  extends JpaRepository <FileModel, UUID> {


}
