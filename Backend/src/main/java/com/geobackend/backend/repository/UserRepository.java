package com.geobackend.backend.repository;

import com.geobackend.backend.entities.Utilisateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<Utilisateur, Long> {
	public Utilisateur findByEmail(String email);

}
